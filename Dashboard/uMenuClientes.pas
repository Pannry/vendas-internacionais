unit uMenuClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uMenu, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.ImageList,
  Vcl.ImgList, Vcl.DBCGrids, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.WinXCtrls,
  Vcl.ExtCtrls, Vcl.CategoryButtons, Vcl.Grids, Vcl.DBGrids;

type
  TfrmMenuClientes = class(TfrmMenu)
    fdQueryCUSTOMER: TStringField;
    fdQueryPHONE_NO: TStringField;
    fdQueryCOUNTRY: TStringField;
    fdQueryCONTACT_FIRST: TStringField;
    fdQueryCONTACT_LAST: TStringField;
    fdQueryADDRESS_LINE1: TStringField;
    fdQueryCITY: TStringField;
    procedure DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure CategoryButtons1Categories0Items0Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items3Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items4Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items5Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenuClientes: TfrmMenuClientes;

implementation

{$R *.dfm}

procedure TfrmMenuClientes.CategoryButtons1Categories0Items0Click(
  Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmMenuClientes.CategoryButtons1Categories0Items3Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet1;
end;

procedure TfrmMenuClientes.CategoryButtons1Categories0Items4Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet2;
end;

procedure TfrmMenuClientes.CategoryButtons1Categories0Items5Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet3;
end;

procedure TfrmMenuClientes.CategoryButtons1Categories0Items6Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet4;
end;

procedure TfrmMenuClientes.DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid;
  Index: Integer);
begin
  inherited;
  Label3.Caption := fdQueryCUSTOMER.AsString;
  Label5.Caption := fdQueryPHONE_NO.AsString;
  Label9.Caption := fdQueryCOUNTRY.AsString;
end;

end.
