unit uMenuVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uMenu, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCGrids, Vcl.ComCtrls,
  Vcl.WinXCtrls, Vcl.CategoryButtons;

type
  TfrmMenuVendas = class(TfrmMenu)
    fdQueryCUST_NO: TIntegerField;
    fdQueryORDER_STATUS: TStringField;
    fdQueryORDER_DATE: TSQLTimeStampField;
    fdQueryPAID: TStringField;
    fdQueryTOTAL_VALUE: TCurrencyField;
    fdQueryITEM_TYPE: TStringField;
    procedure CategoryButtons1Categories0Items3Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items4Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items0Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenuVendas: TfrmMenuVendas;

implementation

{$R *.dfm}

procedure TfrmMenuVendas.CategoryButtons1Categories0Items0Click(
  Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmMenuVendas.CategoryButtons1Categories0Items3Click(Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet1;
end;

procedure TfrmMenuVendas.CategoryButtons1Categories0Items4Click(Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet2;
end;

end.
