unit uMenuPaises;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uMenu, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCGrids, Vcl.ComCtrls,
  Vcl.WinXCtrls, Vcl.CategoryButtons;

type
  TfrmMenuPaises = class(TfrmMenu)
    fdQueryCOUNTRY: TStringField;
    fdQueryCURRENCY: TStringField;
    procedure CategoryButtons1Categories0Items0Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items3Click(Sender: TObject);
    procedure CategoryButtons1Categories0Items4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenuPaises: TfrmMenuPaises;

implementation

{$R *.dfm}

procedure TfrmMenuPaises.CategoryButtons1Categories0Items0Click(
  Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmMenuPaises.CategoryButtons1Categories0Items3Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet1;
end;

procedure TfrmMenuPaises.CategoryButtons1Categories0Items4Click(
  Sender: TObject);
begin
  inherited;
  pcPrincipal.ActivePage := TabSheet2;
end;

end.
