program dashboard;

uses
  Vcl.Forms,
  uMenu in 'uMenu.pas' {frmMenu},
  uPrincipal in 'uPrincipal.pas' {frmPrincipal},
  uDM in 'uDM.pas' {DataModule1: TDataModule},
  uMenuClientes in 'uMenuClientes.pas' {frmMenuClientes},
  uMenuVendas in 'uMenuVendas.pas' {frmMenuVendas},
  uMenuPaises in 'uMenuPaises.pas' {frmMenuPaises};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
